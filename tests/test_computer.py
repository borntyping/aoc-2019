import pytest

from advent.computer import Computer, Instruction, Memory, Mode


@pytest.mark.parametrize(
    ("parameter", "instruction"),
    (
        (1002, Instruction(2, Mode.POSITION, Mode.IMMEDIATE, Mode.POSITION)),
        (1002, Instruction(2, Mode.POSITION, Mode.IMMEDIATE, Mode.POSITION)),
    ),
)
def test_opcode(parameter: int, instruction: Instruction) -> None:
    assert Instruction.parse(parameter) == instruction


@pytest.mark.parametrize(
    ("memory", "expected"),
    (
        ([1, 0, 0, 0, 99], [2, 0, 0, 0, 99]),
        ([2, 3, 0, 3, 99], [2, 3, 0, 6, 99]),
        ([2, 4, 4, 5, 99, 0], [2, 4, 4, 5, 99, 9801]),
        ([1, 1, 1, 4, 99, 5, 6, 0, 99], [30, 1, 1, 4, 2, 5, 6, 0, 99]),
        ([1002, 4, 3, 4, 33], [1002, 4, 3, 4, 99]),
        ([1101, 100, -1, 4, 0], [1101, 100, -1, 4, 99]),
    ),
)
def test_add_mul(memory: Memory, expected: Memory):
    assert Computer(memory).main().memory == expected
