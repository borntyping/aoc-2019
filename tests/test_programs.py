import typing

import pytest

from advent.computer import Computer, Memory

PROGRAMS = {
    # The program 3,0,4,0,99 outputs whatever it gets as input, then halts.
    "day_5_part_1_a": "3,0,4,0,99",
    # For example, here are several programs that take one input, compare it to the value 8, and then produce one output:
    # Using position mode, consider whether the input is equal to 8; output 1 (if it is) or 0 (if it is not).
    "day_5_part_2_a": "3,9,8,9,10,9,4,9,99,-1,8",
    # Using position mode, consider whether the input is less than 8; output 1 (if it is) or 0 (if it is not).
    "day_5_part_2_b": "3,9,7,9,10,9,4,9,99,-1,8",
    # Using immediate mode, consider whether the input is equal to 8; output 1 (if it is) or 0 (if it is not).
    "day_5_part_2_c": "3,3,1108,-1,8,3,4,3,99",
    # Using immediate mode, consider whether the input is less than 8; output 1 (if it is) or 0 (if it is not).
    "day_5_part_2_d": "3,3,1107,-1,8,3,4,3,99",
    # Here are some jump tests that take an input, then output 0 if the input was zero or 1 if the input was non-zero:
    "day_5_part_2_e": "3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9",
    "day_5_part_2_f": "3,3,1105,-1,9,1101,0,0,12,4,12,99,1",
    # The [below] example program uses an input instruction to ask for a single number. The
    # program will then output 999 if the input value is below 8, output 1000 if the input
    # value is equal to 8, or output 1001 if the input value is greater than 8.
    "day_5_part_2_g": "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104, 999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99",
}


@pytest.mark.parametrize(
    ("program", "inputs", "outputs"),
    (
        ("day_5_part_1_a", [1], [1]),
        ("day_5_part_1_a", [2], [2]),
        ("day_5_part_2_a", [8], [1]),
        ("day_5_part_2_a", [0], [0]),
        ("day_5_part_2_b", [7], [1]),
        ("day_5_part_2_b", [8], [0]),
        ("day_5_part_2_c", [8], [1]),
        ("day_5_part_2_c", [0], [0]),
        ("day_5_part_2_d", [8], [0]),
        ("day_5_part_2_d", [0], [1]),
        ("day_5_part_2_e", [0], [0]),
        ("day_5_part_2_e", [1], [1]),
        ("day_5_part_2_f", [0], [0]),
        ("day_5_part_2_f", [1], [1]),
        ("day_5_part_2_g", [7], [999]),
        ("day_5_part_2_g", [8], [1000]),
        ("day_5_part_2_g", [9], [1001]),
    ),
)
def test_io(program: str, inputs: Memory, outputs: Memory) -> None:
    memory: Memory = Computer.parse_memory(PROGRAMS[program])
    assert Computer(memory, inputs=inputs).main().outputs == outputs
    raise
