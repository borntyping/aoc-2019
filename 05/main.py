from advent.computer import Computer


def main():
    Computer.setup(__file__, inputs=[1]).main()
    Computer.setup(__file__, inputs=[5]).main()


if __name__ == "__main__":
    main()
