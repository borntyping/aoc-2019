from __future__ import annotations

import dataclasses
import enum
import logging
import operator
import typing

from .debugger import Debugger, result
from .input import load_text

log = logging.getLogger(__name__)

Memory = typing.MutableSequence[int]


class Halt(Exception):
    pass


@enum.unique
class Mode(enum.Enum):
    POSITION = 0
    IMMEDIATE = 1


@dataclasses.dataclass()
class Instruction:
    opcode: int
    a: Mode
    b: Mode
    c: Mode

    @classmethod
    def parse(cls, parameter: int):
        digits = f"{parameter:05}"
        opcode = int(digits[3:5])
        a = int(digits[2])
        b = int(digits[1])
        c = int(digits[0])
        instruction = cls(opcode=opcode, a=Mode(a), b=Mode(b), c=Mode(c))
        return instruction


@dataclasses.dataclass()
class Computer:
    memory: Memory

    inputs: Memory = dataclasses.field(default_factory=list)
    outputs: Memory = dataclasses.field(default_factory=list, init=False)

    execution_limit: int = dataclasses.field(default=1000)
    instruction_count: int = dataclasses.field(default=0, init=False)
    instruction_pointer: int = dataclasses.field(default=0, init=False)
    debugger: Debugger = dataclasses.field(default_factory=Debugger)

    INSTRUCTIONS = {
        1: "add",
        2: "mul",
        3: "input",
        4: "output",
        5: "jump_if_true",
        6: "jump_if_false",
        7: "less_than",
        8: "equals",
        99: "halt",
    }

    @classmethod
    def setup(cls, filename: str, **kwargs) -> Computer:
        return cls(memory=cls.parse_memory(load_text(filename)), **kwargs)

    @classmethod
    def create(cls, text: str, inputs: Memory):
        return cls(memory=cls.parse_memory(text), inputs=inputs)

    @classmethod
    def parse_memory(cls, text: str) -> Memory:
        return [int(x) for x in text.split(",")]

    def get(self, location: int, mode: Mode, *, name: str = "memory") -> int:
        """Access memory using position or immediate parameter mode."""
        assert location >= 0
        value = self.memory[location]
        self.debugger.exc(f"Get {name} in {mode.name} mode", location, value)
        if mode == mode.POSITION:
            return self.get(value, mode=Mode.IMMEDIATE, name=name)
        elif mode == mode.IMMEDIATE:
            return value
        raise NotImplementedError(f"Unknown mode {mode!r}")

    def set(
        self,
        location: int,
        value: int,
        *,
        mode: Mode = Mode.IMMEDIATE,
        name: str = "memory",
    ) -> None:
        assert value is not None
        if mode == mode.POSITION:
            self.set(self.memory[location], value, mode=Mode.IMMEDIATE, name=name)
            return
        elif mode == mode.IMMEDIATE:
            self.debugger.exc(
                f"Set {name} in {mode.name} mode", location, value, fg="cyan"
            )
            self.memory[location] = value
            return
        raise NotImplementedError(f"Unknown mode {mode!r}")

    def instruction(self):
        """Access the current instruction."""
        instruction = Instruction.parse(
            self.get(
                location=self.instruction_pointer,
                mode=Mode.IMMEDIATE,
                name="instruction",
            )
        )
        self.debugger.exc(
            f"Execute '{self.INSTRUCTIONS[instruction.opcode]}'",
            location=self.instruction_pointer,
            value=instruction.opcode,
            fg="green",
        )
        return instruction

    def set_instruction_pointer(self, value: int) -> None:
        self.instruction_pointer = value
        self.debugger.exc(
            f"Set instruction pointer",
            location=self.instruction_pointer,
            value=value,
            fg="green",
        )
        self.debugger.set_instruction_pointer(self.instruction_pointer)

    def offset_instruction_pointer(self, offset: int) -> None:
        self.instruction_pointer += offset
        self.debugger.exc(
            f"Offset instruction pointer [{offset:+}]",
            location=self.instruction_pointer,
            value=offset,
            fg="green",
        )
        self.debugger.set_instruction_pointer(self.instruction_pointer)

    def parameters(self, *modes: Mode):
        for i, mode in enumerate(modes, start=1):
            yield self.get(self.instruction_pointer + i, mode, name=f"parameter {i}")

    def operator(self, A: Mode, B: Mode, op: typing.Callable[[int, int], int]) -> None:
        a, b, c = self.parameters(A, B, Mode.IMMEDIATE)
        self.set(c, op(a, b))
        self.offset_instruction_pointer(4)

    def opcode_1_add(self, A: Mode, B: Mode):
        """
        Opcode 1 adds together numbers read from two positions and stores the result in
        a third position. The three integers immediately after the instruction tell you
        these three positions - the first two indicate the positions from which you
        should read the input values, and the third indicates the position at which the
        output should be stored.
        """
        return self.operator(A, B, operator.add)

    def opcode_2_mul(self, A: Mode, B: Mode):
        """
        Opcode 2 works exactly like instruction 1, except it multiplies the two inputs
        instead of adding them. Again, the three integers after the instruction indicate
        where the inputs and outputs are, not their values.
        """
        return self.operator(A, B, operator.mul)

    def opcode_3_input(self) -> None:
        """
        Opcode 3 takes a single integer as input and saves it to the position given by
        its only parameter. For example, the add_mul 3,50 would take an input value and
        store it at address 50.
        """
        value = self.inputs.pop(0)
        self.debugger.exc("Pull input value", value=value, fg="magenta")
        a, = self.parameters(Mode.IMMEDIATE)
        self.set(a, value)
        self.offset_instruction_pointer(2)

    def opcode_4_output(self, A: Mode) -> None:
        """
        Opcode 4 outputs the value of its only parameter.
        For example, the instruction 4,50 would output the value at address 50.
        """
        a, = self.parameters(A)
        self.outputs.append(a)
        self.debugger.exc("Push output value", value=a, fg="magenta")
        self.offset_instruction_pointer(2)

    def opcode_5_jump_if_true(self, A: Mode, B: Mode) -> None:
        """
        Opcode 5 is jump-if-true: if the first parameter is non-zero, it sets the
        instruction pointer to the value from the second parameter. Otherwise, it does
        nothing.
        """
        a, b = self.parameters(A, B)
        if a != 0:
            return self.set_instruction_pointer(b)
        else:
            return self.offset_instruction_pointer(3)

    def opcode_6_jump_if_false(self, A: Mode, B: Mode) -> None:
        """
        Opcode 6 is jump-if-false: if the first parameter is zero, it sets the
        instruction pointer to the value from the second parameter. Otherwise, it does
        nothing.
        """
        a, b = self.parameters(A, B)
        if a == 0:
            return self.set_instruction_pointer(b)
        else:
            return self.offset_instruction_pointer(3)

    def opcode_7_less_than(self, A: Mode, B: Mode) -> None:
        """
        Opcode 7 is less than: if the first parameter is less than the second
        parameter, it stores 1 in the position given by the third parameter.
        Otherwise, it stores 0.
        """
        a, b, c = self.parameters(A, B, Mode.IMMEDIATE)
        self.set(c, 1 if a < b else 0)
        self.offset_instruction_pointer(4)

    def opcode_8_equals(self, A: Mode, B: Mode) -> None:
        """
        Opcode 8 is equals: if the first parameter is equal to the second
        parameter, it stores 1 in the position given by the third parameter.
        Otherwise, it stores 0
        """
        a, b, c = self.parameters(A, B, Mode.IMMEDIATE)
        self.set(c, 1 if a == b else 0)
        self.offset_instruction_pointer(4)

    def opcode_99_halt(self):
        """Opcode 99 means that the program is finished and should immediately halt."""
        raise Halt()

    def step(self) -> None:
        """Apply a single instruction."""
        instruction = self.instruction()
        if instruction.opcode == 1:
            return self.opcode_1_add(instruction.a, instruction.b)
        elif instruction.opcode == 2:
            return self.opcode_2_mul(instruction.a, instruction.b)
        elif instruction.opcode == 3:
            return self.opcode_3_input()
        elif instruction.opcode == 4:
            return self.opcode_4_output(instruction.a)
        elif instruction.opcode == 5:
            return self.opcode_5_jump_if_true(instruction.a, instruction.b)
        elif instruction.opcode == 6:
            return self.opcode_6_jump_if_false(instruction.a, instruction.b)
        elif instruction.opcode == 7:
            return self.opcode_7_less_than(instruction.a, instruction.b)
        elif instruction.opcode == 8:
            return self.opcode_8_equals(instruction.a, instruction.b)
        elif instruction.opcode == 99:
            return self.opcode_99_halt()
        raise NotImplementedError(str(instruction))

    def main(self) -> Computer:
        self.debugger.start()
        try:
            for i in range(self.execution_limit):
                self.debugger.set_instruction_count(i)
                self.step()
                self.debugger.step()
            self.debugger.stop("Execution limit")
        except Halt:
            self.debugger.stop("Halted execution", f"Outputs: {self.outputs}")
        except Exception as error:
            self.debugger.error(error)
            raise
        return self
