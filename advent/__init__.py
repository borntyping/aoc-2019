import logging

import colorlog


def setup():
    colorlog.basicConfig(level=logging.INFO)
