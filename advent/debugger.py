import dataclasses
import enum
import itertools
import textwrap
import typing

import click


@dataclasses.dataclass(frozen=True)
class Table:
    columns: typing.Sequence[int]

    @property
    def width(self):
        return sum(self.columns) + len(self.columns) - 1

    @staticmethod
    def row(
        parts: typing.Sequence[typing.Optional[str]],
        columns: typing.Sequence[int],
        style: typing.Mapping[str, typing.Any],
    ):
        assert len(parts) <= len(columns), f"{len(parts)} <= {len(columns)}"
        x = itertools.zip_longest(columns, parts)
        x = ((width, part if part else "") for width, part in x)
        x = ((width, f" {part} ") for width, part in x)
        x = (f"{part:{width}}" for width, part in x)
        x = (click.style(part, **style) for part in x)
        click.secho(f"│{'│'.join(x)}│", color=True)

    def line(self, *parts: typing.Optional[str], **style):
        return self.row(parts, self.columns, style)

    def top(self, left: str = "┌", center: str = "┬", right: str = "┐"):
        return self.divider(left=left, center=center, right=right)

    def middle(self, left: str = "├", center: str = "┼", right: str = "┤"):
        return self.divider(left=left, center=center, right=right)

    def bottom(self, left: str = "└", center: str = "┴", right: str = "┘"):
        return self.divider(left=left, center=center, right=right)

    def line_wide(self, part: str, **style):
        return self.row([part], [self.width], style)

    def middle_end(self, left: str = "├", center: str = "┴", right: str = "┤"):
        return self.divider(left=left, center=center, right=right)

    def bottom_end(self, left: str = "└", center: str = "─", right: str = "┘"):
        return self.divider(left=left, center=center, right=right)

    def section_end(self, *messages: str, **style):
        self.middle_end()
        for message in messages:
            for line in textwrap.wrap(message, self.width):
                self.line_wide(line, **style)
        self.bottom_end()

    def divider(self, left: str, center: str, right: str):
        parts = ["─" * width for width in self.columns]
        click.secho(f"{left}{f'{center}'.join(parts)}{right}", color=True)


@dataclasses.dataclass()
class Debugger:
    table: Table = Table((10, 45, 10, 10))
    instruction_count: int = 0
    instruction_pointer: int = 0

    def header(self):
        self.table.line("Pointer", "Description", "Location", "Value", dim=True)

    def start(self) -> None:
        self.table.top()
        self.header()
        self.table.middle()

    def step(self) -> None:
        self.table.middle()

    def stop(self, *args) -> None:
        self.table.middle()
        self.header()
        self.table.section_end(*args, fg="red")

    def error(self, exception: Exception) -> None:
        self.table.section_end(str(exception), fg="red", bold=True)

    def exc(
        self,
        action: str,
        location: typing.Optional[int] = None,
        value: typing.Optional[int] = None,
        fg: typing.Optional[str] = None,
    ):
        self.table.line(
            self.format_value(self.instruction_pointer),
            action,
            self.format_value(location) if location is not None else None,
            self.format_value(value) if value is not None else None,
            fg=fg,
        )

    @staticmethod
    def format_mode(mode: enum.Enum) -> str:
        return mode.name

    @staticmethod
    def format_value(value: int) -> str:
        string = str(value)
        length = len(string)
        if length > 8:
            return click.style("!!!!!!!!", fg="red")
        prefix = click.style("0" * (8 - length), dim=True, reset=False)
        string = click.style(string, dim=False, reset=False)
        return f"{prefix + string:8}"

    def set_instruction_count(self, i: int) -> None:
        self.instruction_count = i

    def set_instruction_pointer(self, i: int) -> None:
        self.instruction_pointer = i


def result(*results: typing.Tuple[str, typing.Any]) -> None:
    table = Table((20, 53))
    table.top()
    for name, value in results:
        table.line(name, str(value), fg="green")
    table.bottom()
