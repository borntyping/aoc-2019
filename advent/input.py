import pathlib


def load_text(filename: str):
    path = pathlib.Path(filename).with_name("input.txt")
    return path.read_text().strip()
