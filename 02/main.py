from advent.computer import Computer
from advent import setup
from advent.debugger import result


def part1():
    computer = Computer.setup(__file__)
    computer.memory[1] = 12
    computer.memory[2] = 2
    computer.main()
    result(("Result", computer.memory[0]))


def part2(noun: int, verb: int) -> int:
    computer = Computer.setup(__file__)
    computer.memory[1] = noun
    computer.memory[2] = verb
    computer.main()
    return computer.memory[0]


def part2_search():
    for noun in range(0, 99):
        for verb in range(0, 99):
            if part2(noun, verb) == 19690720:
                result(("Result", 100 * noun + verb))
                return


if __name__ == "__main__":
    setup()
    part1()
    part2_search()
