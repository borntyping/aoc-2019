from __future__ import annotations

import dataclasses
import enum
import math
import typing

import pytest

import logging

from advent import setup
from advent.input import load_text

log = logging.getLogger(__name__)


@dataclasses.dataclass(frozen=True)
class Point:
    x: int
    y: int


@enum.unique
class Direction(enum.Enum):
    UP = "U"
    DOWN = "D"
    LEFT = "L"
    RIGHT = "R"


@dataclasses.dataclass()
class Instruction:
    direction: Direction
    distance: int

    def translation(self) -> typing.Tuple[int, int]:
        if self.direction == Direction.UP:
            return 0, self.distance
        elif self.direction == Direction.DOWN:
            return 0, -self.distance
        elif self.direction == Direction.LEFT:
            return -self.distance, 0
        elif self.direction == Direction.RIGHT:
            return self.distance, 0
        raise NotImplementedError


@dataclasses.dataclass()
class Line:
    origin: Point
    finish: Point

    def intersections(self, other: Line) -> typing.Set[Point]:
        s = set(self.points())
        o = set(other.points())
        return s.intersection(o)

    def points(self) -> typing.Sequence[Point]:
        xstep = 1 if self.origin.x <= self.finish.x else -1
        ystep = 1 if self.origin.y <= self.finish.y else -1
        xs = tuple(range(self.origin.x, self.finish.x + 1, xstep))
        ys = tuple(range(self.origin.y, self.finish.y + 1, ystep))
        return [Point(x=x, y=y) for x in xs for y in ys]


def parse(text: str) -> typing.Sequence[Instruction]:
    return [Instruction(Direction(i[0]), int(i[1:])) for i in text.split(",")]


def lines(instructions: typing.Sequence[Instruction]) -> typing.Sequence[Line]:
    origin = Point(0, 0)
    for i, instruction in enumerate(instructions):
        log.info(f"Creating line for add_mul {i}")
        dx, dy = instruction.translation()
        finish = Point(origin.x + dx, origin.y + dy)
        yield Line(origin, finish)
        origin = finish


def intersections(
    first: typing.Sequence[Line], second: typing.Sequence[Line]
) -> typing.Sequence[Point]:
    for fi, f in enumerate(first):
        log.info(f"{fi} of {len(first)} lines")
        for si, s in enumerate(second):
            yield from f.intersections(s)


def closest(first_text: str, second_text: str) -> int:
    first_lines = tuple(lines(parse(first_text)))
    second_lines = tuple(lines(parse(second_text)))
    points = set(intersections(first_lines, second_lines))
    points = points - {Point(0, 0)}
    log.info(f"points={points}")
    return min(abs(p.x) + abs(p.y) for p in points)


def lowest_delay(first_text: str, second_text: str) -> int:
    first_lines = tuple(lines(parse(first_text)))
    second_lines = tuple(lines(parse(second_text)))
    intersection_points = set(intersections(first_lines, second_lines))
    intersection_points = intersection_points - {Point(0, 0)}

    first_line = [p for line in first_lines for p in line.points()]
    second_line = [p for line in second_lines for p in line.points()]

    return (
        min(
            first_line.index(point) + second_line.index(point)
            for point in intersection_points
        )
        - 4
    )


def test_parse():
    assert parse("R75,D30,R83,U83,L12,D49,R71,U7") == [
        Instruction(direction=Direction("R"), distance=75),
        Instruction(direction=Direction("D"), distance=30),
        Instruction(direction=Direction("R"), distance=83),
        Instruction(direction=Direction("U"), distance=83),
        Instruction(direction=Direction("L"), distance=12),
        Instruction(direction=Direction("D"), distance=49),
        Instruction(direction=Direction("R"), distance=71),
        Instruction(direction=Direction("U"), distance=7),
    ]


def test_lines():
    assert tuple(lines(parse("R75,D30,R83,U83,L12,D49,R71,U7"))) == (
        Line(Point(x=0, y=0), Point(x=75, y=0)),
        Line(Point(x=75, y=0), Point(x=75, y=-30)),
        Line(Point(x=75, y=-30), Point(x=158, y=-30)),
        Line(Point(x=158, y=-30), Point(x=158, y=53)),
        Line(Point(x=158, y=53), Point(x=146, y=53)),
        Line(Point(x=146, y=53), Point(x=146, y=4)),
        Line(Point(x=146, y=4), Point(x=217, y=4)),
        Line(Point(x=217, y=4), Point(x=217, y=11)),
    )


@pytest.mark.parametrize(
    ("first", "second", "distance"),
    (
        ("R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83", 159),
        (
            "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51",
            "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7",
            135,
        ),
    ),
)
def test_distance(first: str, second: str, distance: int):
    assert closest(first, second) == distance


@pytest.mark.parametrize(
    ("first", "second", "distance"),
    (
        ("R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83", 610),
        (
            "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51",
            "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7",
            410,
        ),
    ),
)
def test_delay(first: str, second: str, distance: int):
    assert lowest_delay(first, second) == distance


def main():
    f, s = load_text(__file__).splitlines()
    print(f"Result = {lowest_delay(f, s)}")


if __name__ == "__main__":
    setup()
    main()
