import dataclasses
import itertools

import pytest
import logging

import typing

log = logging.getLogger(__name__)


@dataclasses.dataclass()
class Cracker:
    minimum: int
    maximum: int

    def check(self, integer: int) -> typing.Optional[str]:
        string = str(integer)
        between = self.minimum < integer < self.maximum

        if len(string) != 6:
            return f"Password {integer} is not 6 characters long"

        if not between:
            return f"Password {integer} is not between min and max"

        never_decreases = True

        previous_character = string[0]
        for current_character in string[1:]:
            if int(current_character) < int(previous_character):
                never_decreases = False
            previous_character = current_character

        seen = []
        for character in string:
            if seen and seen[-1][0] == character:
                seen[-1].append(character)
            else:
                seen.append([character])

        sets = [len(c) for c in seen]
        if 2 not in sets:
            return f"Password {integer} has no character pair"

        if never_decreases is False:
            return f"Password {integer} has two characters that increase"

        return None

    def main(self) -> int:
        log.warning(f"Max result: {self.maximum - self.minimum}")
        return sum(1 for x in range(self.minimum, self.maximum) if not self.check(x))


@pytest.mark.parametrize("password", (111122,))
def test_valid(password: int) -> None:
    assert not Cracker(0, 1000000).check(password)


@pytest.mark.parametrize("password", (223450, 123789))
def test_invalid(password: int) -> None:
    assert Cracker(0, 1000000).check(password)


if __name__ == "__main__":
    logging.basicConfig()
    log.warning(f"Result = {Cracker(264360, 746325).main()}")
