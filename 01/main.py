import logging
import math
import pathlib

import colorlog
import pytest

log = colorlog.getLogger(__name__)


def fuel(mass: int) -> int:
    """
    To find the fuel required for a module,
    take its mass,
    divide by three,
    round down,
    and subtract 2.
    """
    return math.floor(mass / 3) - 2


def fuel_fuel(mass: int) -> int:
    fuels = [fuel(mass)]

    while True:
        extra = fuel(fuels[-1])
        if extra > 0:
            fuels.append(extra)
        else:
            break

    return sum(fuels)


@pytest.mark.parametrize(("mass", "total"), [(14, 2), (1969, 966)])
def test_fuel_fuel(mass: int, total: int):
    """
    1969 -> 654 + 216 + 70 + 21 + 5 = 966
    """
    assert fuel_fuel(mass) == total


def main():
    colorlog.basicConfig(level=logging.DEBUG)
    path = pathlib.Path(__file__).with_name("input.txt")
    lines = path.read_text().splitlines()
    modules = [int(line.strip()) for line in lines]
    result = sum(fuel_fuel(mass) for mass in modules)
    print(f"\nResult: {result}")


if __name__ == "__main__":
    main()
